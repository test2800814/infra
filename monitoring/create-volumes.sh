mkdir -p /var/data/thanos-store

sudo chown -R root:root /var/data/thanos-store
sudo chmod -R 755 /var/data/thanos-store
sudo chown -R 1000:1000 /var/data/thanos-store

mkdir -p /var/data/prometheus

sudo chown -R root:root /var/data/prometheus
sudo chmod -R 755 /var/data/prometheus
sudo chown -R 1000:1000 /var/data/prometheus

mkdir -p /var/data/loki

sudo chown -R root:root /var/data/loki
sudo chmod -R 755 /var/data/loki
sudo chown -R 1000:1000 /var/data/loki