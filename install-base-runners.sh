#!/bin/sh

# Check if tokens are provided
if [ "$#" -ne 2 ]; then
  echo "Usage: $0 <docker_runner_token> <shell_runner_token>"
  exit 1
fi

docker_runner_token="$1"
shell_runner_token="$2"

curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | sudo bash
sudo apt-get install gitlab-runner

sudo usermod -aG docker gitlab-runner

gitlab-runner register \
  --non-interactive \
  --url https://gitlab.com \
  --name docker-runner \
  --executor docker \
  --docker-image maven:3.8.4-openjdk-17-slim \
  --docker-volumes /run/docker.sock:/var/run/docker.sock \
  --token $docker_runner_token

gitlab-runner register \
  --non-interactive \
  --url https://gitlab.com \
  --name shell-runner \
  --executor shell \
  --token $shell_runner_token