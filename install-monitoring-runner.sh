#!/bin/sh

# Check if token is provided
if [ "$#" -ne 1 ]; then
  echo "Error: Incorrect number of arguments."
  echo "Usage: $0 <monitoring_runner_token>"
  exit 1
fi

monitoring_runner_token="$1"

curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | sudo bash
sudo apt-get install gitlab-runner

sudo usermod -aG docker gitlab-runner

gitlab-runner register \
  --non-interactive \
  --url https://gitlab.com \
  --name monitoring-runner \
  --executor shell \
  --token $monitoring_runner_token
